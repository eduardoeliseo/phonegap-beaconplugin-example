//
//  BeaconPlugin.h
//  BeaconManager
//
//  Created by Eduardo on 15/2/16.
//
//

#import <Cordova/CDV.h>
#import "BeaconManager.h"

@interface BeaconPlugin : CDVPlugin <BeaconManagerDelegate>

@property (strong, nonatomic) BeaconManager *beaconManager;

-(void) startMonitoringAndRanging:(CDVInvokedUrlCommand *)command;

-(void) stopMonitoringAndRanging:(CDVInvokedUrlCommand *)command;

@end
