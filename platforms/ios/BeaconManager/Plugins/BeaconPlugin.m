//
//  BeaconPlugin.m
//  BeaconManager
//
//  Created by Eduardo on 15/2/16.
//
//

#import "BeaconPlugin.h"

@implementation BeaconPlugin

#pragma mark - Callback methods

-(void) startMonitoringAndRanging:(CDVInvokedUrlCommand *)command {
    
    [self.commandDelegate runInBackground:^{
        
        [self.beaconManager startMonitoringAndRanging];
        
        CDVPluginResult* pluginResult;
        if (self.beaconManager.errorData) {
            NSString *message = [self.beaconManager.errorData.userInfo valueForKey:NSLocalizedDescriptionKey];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
        }
        else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Ranging and monitoring is ON!"];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

-(void) stopMonitoringAndRanging:(CDVInvokedUrlCommand *)command {
    
    [self.commandDelegate runInBackground:^{
        [self.beaconManager stopMonitoringAndRanging];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Status: OFF"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

#pragma mark - Initializer/lazy getter

-(BeaconManager *) beaconManager {
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (!_beaconManager) {
            _beaconManager = [[BeaconManager alloc] init];
            _beaconManager.managerDelegate = self;
        }
    });
    
    return _beaconManager;
}

#pragma mark - BeaconManagerDelegate
/* Comment/uncomment the methods that will be used */

-(void) locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    //NSLog(@"monitoringDidFailForRegion:%@ withError:%@",region,error.userInfo);
}

-(void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
    
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    //NSLog(@"rangingBeaconsDidFailForRegion:%@ withError:%@",region,error.userInfo);
}

-(void) locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    //TODO: implement js call here
    /*
    [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
    }];
     */
    
    //NSLog(@"Did fail with error: %@", error);
}

-(void) locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray<CLBeacon *> *)beacons
               inRegion:(CLBeaconRegion *)region
{
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    
    // Example
    [self.commandDelegate runInBackground:^{
        CLBeacon *closestBeacon = [self.beaconManager.beaconsDetected firstObject];
        NSString *js;
        if (closestBeacon) {
            // Example js definition
            js = [NSString stringWithFormat:@"didRange('major:%@, minor:%@ - [%@]')",closestBeacon.major,closestBeacon.minor, [BeaconManager getProximityStringOfBeacon:closestBeacon]];
        } else {
            // Example js definition
             js = [NSString stringWithFormat:@"didRange('...')"];
        }
        // Example js call
        [self.commandDelegate evalJs:js];
    }];
}

-(void) locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    //NSLog(@"Did start monitoring the region: %@", region.identifier);
}

-(void) locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region
{
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    //NSLog(@"Did enter region: %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region
{
    //TODO: implement js call here
    /*
     [self.commandDelegate runInBackground:^{
        //some logic here...
        //[self.commandDelegate evalJs:@"someJsFunction(params...)"]
     }];
     */
    //NSLog(@"Did exit region: %@", region.identifier);
}

#pragma mark - CDVPlugin

-(void) onAppTerminate {
    //TODO: implement some logic for when the app is terminated
    [self.beaconManager stopMonitoringAndRanging];
}

@end
