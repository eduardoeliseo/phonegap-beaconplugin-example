//
//  BeaconManager.h
//
//
//  Created by Eduardo on 9/2/16.
//
//

#import <CoreLocation/CoreLocation.h>

@protocol BeaconManagerDelegate;

@interface BeaconManager : NSObject

@property (strong, nonatomic) NSError *errorData;
@property (assign, nonatomic) BOOL isLocationManagerAuthorizationEnabled;
@property (strong, atomic) NSArray<CLBeacon *> *beaconsDetected;
@property (weak) id<BeaconManagerDelegate> managerDelegate;

-(void) startMonitoringAndRanging;
-(void) stopMonitoringAndRanging;
+(NSString *) getProximityStringOfBeacon:(CLBeacon *)beacon;

@end

@protocol BeaconManagerDelegate <CLLocationManagerDelegate>

@end
